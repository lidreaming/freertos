#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

static void vTask1( void *pvParameters );
static void vTask2( void *pvParameters );

int main()
{
    static xQueueHandle xTestQueue;
    xTaskCreate( vTask1, "vTask1", configMINIMAL_STACK_SIZE, ( void * ) &xTestQueue, tskIDLE_PRIORITY, NULL );
    xTaskCreate( vTask2, "vTask2", configMINIMAL_STACK_SIZE, ( void * ) &xTestQueue, tskIDLE_PRIORITY, NULL );

    vTaskStartScheduler();
    return 1;
}

static void vTask1( void *pvParameters )
{
    unsigned short usValue = 0;

    for( ;; )
    {       
        printf("Task1 hello jamie: [T1] %d\r\n", usValue);
        ++usValue;
        vTaskDelay( 1000 );
    }
}
static void vTask2( void *pvParameters )
{
    unsigned short usValue = 0;

    for( ;; )
    {       
        printf("Task2 hello jamie: [T2] %d\r\n", usValue);
        ++usValue;
        vTaskDelay( 2000 );
    }
}

/********************************************************/
/* This is a stub function for FreeRTOS_Kernel */
void vMainQueueSendPassed( void )
{
    return;
}

/* This is a stub function for FreeRTOS_Kernel */
void vApplicationIdleHook( void )
{
    return;
}





